import zmq,yaml
import os,time,datetime
import bitarray,struct
import common.unpacker as unpacker
from tqdm import tqdm
import argparse
from devices import ke2410


class yaml_config:
    yaml_opt=yaml.YAMLObject()
    
    def __init__(self,fname):
        with open(fname) as fin:
            self.yaml_opt=yaml.safe_load(fin)
            
    def dump(self):
        return yaml.dump(self.yaml_opt)

    def dumpToYaml(self,fname="config.yaml"):
        with open(fname,'w') as fout:
            yaml.dump(self.yaml_opt,fout)
        
if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--config",type=str, help="Name of the configuration file",default="default-config.yaml", required=False)    
    parser.add_argument("--nEvent",type=int, help="number of events",default=1000, required=True)
    parser.add_argument("--preampPolarity",type=int, help="preampPolarity (1: positive, 0: negative)",default=0, required=True)
    parser.add_argument("--leakageCurrentCompPolarity", type=int, help="leakageCurrentCompPolarity (1: positive, 0: negative)",default=0, required=True)
    parser.add_argument("--leakageCurrentComp",type=int, help="leakageCurrentComp (5 bits i.e. 0-31)",default=0, required=True)
    parser.add_argument("--acquisitionType",type=str, help="Possibilities are: 'standard','sweep','fixed','const_inj','instrumental_trigger','external_trigger'",default="-", required=True)    
    parser.add_argument("--rawDataFile",type=str, help="Raw data file path",default="dummy.raw", required=True)    
    parser.add_argument("--configFileCopy",type=str, help="Configuration file path",default="dummy.yaml", required=True)    
    parser.add_argument("--voltages",  type=int, nargs="+", help="Bias voltages to apply", required=True)
    parser.add_argument("--showData",  type=bool, help="Decode and show data during data acquisition", required=False, default=False)
    options = parser.parse_args()
    print(options)

    unsorted_voltage_steps = options.voltages
    sorted_voltage_steps = sorted(unsorted_voltage_steps, key=lambda x: abs(x))
 
    #overwrite options
    conf=yaml_config(options.config)
    if options.acquisitionType!="-":
        conf.yaml_opt['daq_options']['acquisitionType']=options.acquisitionType
    if options.nEvent>0:
        conf.yaml_opt['daq_options']['nEvent']=options.nEvent
    if options.preampPolarity in [0, 1]:
        conf.yaml_opt['daq_options']['preampPolarity']=options.preampPolarity
    if options.leakageCurrentCompPolarity in [0, 1]:
        conf.yaml_opt['daq_options']['leakageCurrentCompPolarity']=options.leakageCurrentCompPolarity
    if options.leakageCurrentComp in range(32):
        conf.yaml_opt['daq_options']['leakageCurrentComp']=options.leakageCurrentComp
    daq_options=conf.yaml_opt['daq_options']

    conf.yaml_opt['glb_options']['outputRawDataPath']=options.rawDataFile
    conf.yaml_opt['glb_options']['outputYamlPath']=options.configFileCopy
    glb_options=conf.yaml_opt['glb_options']

    print("Global options = ",yaml.dump(glb_options))

    if glb_options['startServerManually']==False:
        os.system("ssh -T hgsensor@"+glb_options['serverIpAdress']+" \"nohup python "+glb_options['serverCodePath']+"/daq-zmq-server.py > log.log 2>&1& \"")
    
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    print("Send_String request to server")
    socket.connect("tcp://"+glb_options['serverIpAdress']+":5555")

    dataSize=30786 # 30784 + 2 for injection value
    if daq_options['compressRawData']==True:
            dataSize=15394 # 30784/2 + 2 for injection value
    dataStringUnpacker=struct.Struct('B'*dataSize)

    cmd="DAQ_CONFIG"
    print(cmd)
    socket.send_string(cmd)
    status=socket.recv_string()
    if status=="READY_FOR_CONFIG":
        #the main DAQ needs to be ready to record NEvents x number of voltage steps
        socket.send_string(conf.dump())
        the_config=socket.recv_string()
        print("Returned DAQ_CONFIG:\n%s"%the_config)
    else:
        print("WRONG STATUS -> exit()")
        exit()

    #apply the configuration to the chips
    cmd="CONFIGURE"
    print(cmd)
    socket.send_string(cmd)
    return_bitstring = socket.recv_string()
    print("Returned bit string = %s" % return_bitstring)
    bitstring=[int(i,16) for i in return_bitstring.split()]
    print("\t write bits string in output file")
    byteArray_config = bytearray(bitstring)

    #dump config to file
    if glb_options['storeYamlFile']==True:
        if not os.path.exists(os.path.dirname(glb_options['outputYamlPath'])):
            os.mkdir(os.path.dirname(glb_options['outputYamlPath']))
        yamlFileName=glb_options['outputYamlPath']
        print("save yaml file : ",yamlFileName)
        conf.dumpToYaml(yamlFileName)

    #prepare unpacking if applicable
    if options.showData:
        unpackerInstance = unpacker.unpacker(compressedRawData=daq_options['compressRawData'])

    #prepare the puller
    puller=context.socket(zmq.PULL)
    puller.connect("tcp://"+glb_options['serverIpAdress']+":5556")

    #setup the Keithley 2410
    dev = ke2410(address="GPIB0::24::INSTR")   #address of the KEITHLEY
    dev.print_idn()
    dev.set_source('voltage')
    dev.set_sense('current')
    dev.set_terminal('rear')
    dev.set_current_limit(2000*1e-6)        #2mA compliance
    dev.set_output_on()

    outputFile=0
    for voltage in tqdm(unsorted_voltage_steps, unit="voltage-steps"):
        print()
        print("BIASVOLTAGE:",voltage,"V")
        time.sleep(1)
        dev.ramp_voltage(voltage)
        print()
        print(dev.read_voltage())
        print(dev.read_current())
        print()
        skip_voltage = False

        if not os.path.exists(os.path.dirname(glb_options['outputRawDataPath'])):
            os.mkdir(os.path.dirname(glb_options['outputRawDataPath']))
        rawFileName=glb_options['outputRawDataPath'].replace(".raw", "_%iV.raw"%voltage)
        if os.path.exists(rawFileName):
            print(rawFileName, " already exists and will be deletd")
            os.remove(rawFileName)
        print("open output file : ",rawFileName)
        outputFile = open(rawFileName,'wb')

        if skip_voltage:
            print("Skip %i V"%voltage)
            continue

        #write config to output file
        outputFile.write(byteArray_config)

        # Start processing and pushing of events           
        cmd="PROCESS_AND_PUSH_N_EVENTS"
        socket.send_string(cmd)
        mes=socket.recv_string()
        print(mes)


        try:       
            print("Progression :")
            for i in tqdm(range(0,daq_options['nEvent']), unit="Events"):                  
                str_data=puller.recv()
                rawdata=dataStringUnpacker.unpack(str_data)
                if options.showData:
                    unpackerInstance.unpack(rawdata)
                    unpackerInstance.showData(i+1)
                byteArray = bytearray(rawdata)
                outputFile.write(byteArray)

            
            '''     #T.Quast 23 June 2020: test, does one really need to shutdown the server?, 
            socket.send_string("SHUTDOWN_SERVER")
            if socket.recv_string()=="CLOSING_SERVER":
                socket.close()
                context.term()
            '''
            
        except KeyboardInterrupt:
            print("keyboard interruption")
            os.system("ssh -T pi@"+glb_options['serverIpAdress']+" \" killall python\"")
            dev.ramp_down()
            dev.set_output_off()
            dev.reset()    
            puller.close()     

#close communication to the puller
puller.close()
#turn off power supply            
dev.ramp_down()
dev.set_output_off()
dev.reset()