# ============================================================================
# File: pyvisa_decices.py
# -----------------------
# Communication based ony py-visa.
#
# Date: 04.05.2019
# Author: Florian Pitters
#
# ============================================================================

import socket
import logging


# Base error class
class device_error(object):
    """
    Abstract error class for ethernet devices.
    """
    pass



# Base device class
class device(object):
    """
    Abstract base class for ethernet devices.

    Example:
    dev = device(address=24, port=5005)
    """

    def __init__(self, address=24, port=80):

        ## Set up control
        self.tcp_ip = address
        self.tcp_port = port
        self.ctrl = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        ## Set up logger
        self.logging = logging.getLogger('root')
        self.logging.info("Initialising device.")

        try:
            self.ctrl.connect((self.tcp_ip, self.tcp_port))
            self.logging.info(self.ctrl.query("*IDN?"))
            return 0

        except socket.timeout:
            self.logging.info("Device not found.")
            return  -1


    def idn(self):
        # return self.ctrl.tcp_ip
        return self.ctrl.gethostbyname()

    def open_connection(self):
        return self.ctrl.open()

    def check_connection(self):
        return self.ctrl.isOpen()

    def close_connection(self):
        return self.ctrl.close()

    def query(self, cmd, buffer=1024):
        self.ctrl.send(cmd)
        return self.ctrl.recv(buffer)


    def send(self, cmd):
        nsent = 0
        while nsent < len(cmd):
            sent = self.sock.send(msg[nsent:])
            if sent == 0:
                raise RuntimeError("socket connection broken")
            nsent = nsent + sent

    def receive(self):
        chunks = []
        bytes_recd = 0
        while bytes_recd < len(cmd):
            chunk = self.sock.recv(min(len(cmd) - bytes_recd, 2048))
            if chunk == b'':
                raise RuntimeError("socket connection broken")
            chunks.append(chunk)
            bytes_recd = bytes_recd + len(chunk)
        return b''.join(chunks)
