# Based on F. Pitters pyDAQ implementation for the ARRAY system

from .pyvisa_device import device, device_error
from .ke2410 import ke2410
