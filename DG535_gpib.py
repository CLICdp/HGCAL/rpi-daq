import yaml
from plx_gpib_ethernet import PrologixGPIBEthernet
from time import sleep

class delay535:
    def __init__(self,settings):
        print(settings['connectorIP'])
        self.gpib = PrologixGPIBEthernet( settings['connectorIP'] )
        itry=0
        while True:
            try:
                self.gpib.connect()
                self.gpib.select(15)

            except:
                sleep(0.2)
                itry=itry+1
                if (itry%10)==0:
                    print("still failing to connect to gpib connector after %i tries"%itry)
            
            
            # #trigger of delay generator
            cmd='TM' + str(settings['TM'])
            print(cmd)
            self.gpib.write( cmd )
            cmd='TL' + str(settings['TL'])
            print(cmd)
            self.gpib.write( cmd )

            cmd='TZ0,' + str(settings['TZ0'])
            print(cmd)
            self.gpib.write( cmd )
            
            #output channel B (injection)
            cmd='TZ3,' + str(settings['TZ3'])
            print(cmd)
            self.gpib.write( cmd )
            cmd='OM3,' + str(settings['OM3'])
            print(cmd)
            self.gpib.write( cmd )
            cmd='DT3,1,' + str(settings['DT31'])
            print(cmd)
            self.gpib.write( cmd )
            
            #output channel A (injection)
            cmd='TZ2,' + str(settings['TZ2'])
            print(cmd)
            self.gpib.write( cmd )
            cmd='OM2,' + str(settings['OM2'])
            print(cmd)
            self.gpib.write( cmd )
            cmd='OA2,' + str(settings['OA2'])
            print(cmd)
            self.gpib.write( cmd )
            cmd='DT2,3,' + str(settings['DT23'])
            print(cmd)
            self.gpib.write( cmd )
            print('gpib connection success')
            break
       
    def setDelay23(self,delay):
        cmd='DT2,3,'+str(delay)
        print(cmd)
        self.gpib.write(cmd) 

    
